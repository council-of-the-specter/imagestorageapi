CREATE TABLE account (
  username VARCHAR PRIMARY KEY NOT NULL,
  password VARCHAR NOT NULL,
  accessLevel VARCHAR DEFAULT 'Regular' NOT NULL,
  created_at BIGINT NOT NULL,
  updated_at BIGINT NOT NULL
)