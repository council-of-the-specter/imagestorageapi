use crypto::sha2::Sha256;
use jsonwebtoken as jwt;
use jsonwebtoken::Algorithm;

pub fn verify_token(token: &str) -> bool {
    let v: Vec<&str> = token.rsplit('.').collect();
    let verify = jwt::verify(
        v[0],
        &[v[2], v[1]].join("."),
        super::AUTH_SECRET.as_bytes(),
        Algorithm::HS256,
    );

    return verify.unwrap();
}
