#![feature(async_await)]
#![allow(dead_code, unused_imports)]

#[macro_use]
extern crate diesel;
extern crate dotenv;

use std::env;

use bcrypt::{hash, verify, DEFAULT_COST};
use chrono::{DateTime, Duration, Utc};
use crypto::sha2::Sha256;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenv::dotenv;
use failure::Error;
use http::{Response, StatusCode};
use jsonwebtoken as jwt;
use parking_lot::Mutex;
use serde::Deserialize;
use serde::Serialize;
use std::ops::Add;
use tide::{error::ResultExt, response, App, Context, EndpointResult};

use jsonwebtoken::Algorithm;
use schema::account;
use serde_json::ser::State;
use tide::response::IntoResponse;

pub mod schema;

static AUTH_SECRET: &'static str = "some_secret_key";

//mod user;
mod util;

#[derive(Debug)]
pub enum AccessLevel {
    Regular,
    Admin,
    SuperUser,
}

#[table_name = "account"]
#[derive(Serialize, Deserialize, Insertable)]
pub struct Account {
    username: String,

    #[serde(skip_serializing)]
    password: String,

    accesslevel: String,

    #[serde(skip_serializing)]
    created_at: i64,
    #[serde(skip_serializing)]
    updated_at: i64,
}

#[derive(Deserialize)]
pub struct Login {
    username: String,
    password: String,
}

#[derive(Deserialize)]
pub struct ChangePassword {
    password: String,
    new_password: String,
}

#[derive(Deserialize)]
pub struct CreateAccount {
    username: String,
    password: String,
    accesslevel: String,
}

#[derive(Serialize, Deserialize)]
pub struct TokenJWT {
    username: String,
    access_level: String,
    expires_at: DateTime<Utc>,
}

#[derive(Serialize, Deserialize)]
pub struct TokenResponse {
    token: String,
    expires_at: DateTime<Utc>,
}

impl Default for AccessLevel {
    fn default() -> Self {
        AccessLevel::Regular
    }
}

struct AppState {
    conn: Mutex<PgConnection>,
}

fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url))
}

async fn login(mut cx: Context<(AppState)>) -> EndpointResult {
    let body = &cx.body_json::<Login>().await.client_err()?;
    let conn = cx.state().conn.lock();

    account::table
        .filter(account::username.eq(body.username.clone()))
        .select(account::username)
        .first::<String>(&*conn)
        .map_err(|_| StatusCode::UNAUTHORIZED)?;

    let password_check = account::table
        .filter(account::username.eq(body.username.clone()))
        .select(account::password)
        .first::<String>(&*conn)
        .map_err(|_| StatusCode::UNAUTHORIZED)?;

    let isPasswordValid = verify(body.password.clone(), &password_check).unwrap();

    if !isPasswordValid {
        Err(StatusCode::UNAUTHORIZED)?;
    }

    let token_jwt = TokenJWT {
        username: body.username.clone(),
        access_level: "".to_owned(),
        expires_at: Utc::now().add(Duration::weeks(2)),
    };

    let token =
        jwt::encode(&jwt::Header::default(), &token_jwt, AUTH_SECRET.as_bytes()).map_err(|_| {
            println!("Failed to create jwt token");
            StatusCode::BAD_REQUEST
        })?;

    let _ver = util::verify_token(&token);

    let token_response = TokenResponse {
        token,
        expires_at: token_jwt.expires_at,
    };

    Ok(response::json(token_response))
}

async fn changePassword(mut cx: Context<(AppState)>) -> EndpointResult {
    let body = &cx.body_json::<Login>().await.client_err()?;
    let conn = cx.state().conn.lock();

    account::table
        .filter(account::username.eq(body.username.clone()))
        .select(account::username)
        .first::<String>(&*conn)
        .map_err(|_| StatusCode::UNAUTHORIZED)?;

    let password_check = account::table
        .filter(account::username.eq(body.username.clone()))
        .select(account::password)
        .first::<String>(&*conn)
        .map_err(|_| StatusCode::UNAUTHORIZED)?;

    let isPasswordValid = verify(body.password.clone(), &password_check).unwrap();

    if !isPasswordValid {
        Err(StatusCode::UNAUTHORIZED)?;
    }

    let token_jwt = TokenJWT {
        username: body.username.clone(),
        access_level: "".to_owned(),
        expires_at: Utc::now().add(Duration::weeks(2)),
    };

    let token =
        jwt::encode(&jwt::Header::default(), &token_jwt, AUTH_SECRET.as_bytes()).map_err(|_| {
            println!("Failed to create jwt token");
            StatusCode::BAD_REQUEST
        })?;

    let _ver = util::verify_token(&token);

    let token_response = TokenResponse {
        token,
        expires_at: token_jwt.expires_at,
    };

    Ok(response::json(token_response))
}

async fn create_user(mut cx: Context<AppState>) -> EndpointResult {
    let body = &cx.body_json::<CreateAccount>().await.client_err()?;
    let conn = cx.state().conn.lock();
    let now = Utc::now();

    let account = Account {
        username: body.username.clone(),
        password: hash(body.password.clone(), DEFAULT_COST).unwrap_or("".to_owned()),
        accesslevel: body.accesslevel.clone(),
        created_at: now.timestamp(),
        updated_at: now.timestamp(),
    };

    diesel::insert_into(account::table)
        .values(&account)
        .execute(&*conn)
        .map_err(|_| StatusCode::CONFLICT)?;

    Ok(response::json(account)
        .with_status(StatusCode::CREATED)
        .into_response())
}

fn main() -> Result<(), Error> {
    env_logger::init();

    let mut app = App::with_state(AppState {
        conn: Mutex::new(establish_connection()),
    });

    app.at("/create_user").post(create_user);
    app.at("/login").post(login);
    app.run("127.0.0.1:3000")?;

    Ok(())
}
