table! {
    account (username) {
        username -> Varchar,
        password -> Varchar,
        accesslevel -> Varchar,
        created_at -> Int8,
        updated_at -> Int8,
    }
}
